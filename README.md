# 可拖拽自动排序可修改甘特图

#### 介绍
基于qiu-gantt修改的可拖拽,拖拽后自动排序,可撤回,带编辑的排班图

#### 软件架构
##模块  components/gantt-v
          |--css--gant-v.scss 全局设置css
          |-- gantt-block  背景格子 360px一天
          |-- gantt-drag-block.vue 预留
          |-- gantt-his-mix 历史操作保存和撤回操作
          |-- gantt-mix.js  通用mix模块,基本函数
          |-- gantt-row  显示行
          |-- gantt-time-block 可拖拽时间块
##模拟数据
          fake-data -- airPlaneData.js  getData()函数返回编制的模拟数据
##主页面 pages/Gantt.vue

#### 安装教程

1.  下载代码
2.  npm install
3.  npm run serve

#### 使用说明
<img src="https://gitee.com/kylexy2009/drag-gantt/raw/master/screenshoot/(8C2TEY%5D%5D%7DNF@YUYHOPF$KA.png">

<img src="https://gitee.com/kylexy2009/drag-gantt/raw/master/screenshoot/gantt.gif">

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


